const http = require('http');

onRequest = async (req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/plain' })
    res.write('Hello world');
    res.end();
    console.log("Обработал запрос")
}

http.createServer(onRequest).listen(8001);
console.log("Запуск сервера...");
